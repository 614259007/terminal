<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */  
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_T', 'model');
    }

	public function index()
	{
		$this->load->view('Home');
	}

	public function insert()
	{
$train = array(
	"T_no"=> $this->input->post("t_no"),
	"pl"=> $this->input->post("tpl")

	
);
$origin = array(
		"to_name"=> $this->input->post("to_name"),
	"to_date"=> $this->input->post("to_date")

);	
$bk_p = array(
		"tb_arr"=> $this->input->post("tb_arr"),
	"tb_out"=> $this->input->post("tb_out")

);	

$des = array(
		"td_name"=> $this->input->post("td_name"),
	"td_date"=> $this->input->post("td_date")

);

$this->model->train($train);
$this->model->origin($origin);
$this->model->destination($des);
$this->model->break_point($bk_p);
 $data['query'] = $this->model->select_t();
        $this->load->view('view', $data);
}

public function show()
    {

        $data['query'] = $this->model->select_t();
        $this->load->view('view', $data);
    }

}
