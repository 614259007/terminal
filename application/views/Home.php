<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstrap');
?>
<html>
<style>html {
  height: 100%;
}
body {
  margin:0;
  padding:0;
  font-family: sans-serif;
  background: linear-gradient(#141e30, #141e30);
    color: #fff;

}

.login-box {
  position: absolute;
  top: 50%;
  left: 50%;
  width: 400px;
  padding: 40px;
  transform: translate(-50%, -50%);
  background: rgba(0,0,0,.5);
  box-sizing: border-box;
  box-shadow: 0 15px 25px rgba(0,0,0,.6);
  border-radius: 10px;
}

.login-box h2 {
  margin: 0 0 2px;
  padding: 0;
  color: #fff;
  text-align: center;
}

.login-box .user-box {
  position: relative;
}

.login-box .user-box input {
  width: 100%;
  padding: 10px 0;
  font-size: 16px;
  color: #fff;
  margin-bottom: 30px;
  border: none;
  border-bottom: 1px solid #fff;
  outline: none;
  background: transparent;
}
.login-box .user-box label {
  position: absolute;
  top:0;
  left: 0;
  padding: 10px 0;
  font-size: 16px;
  color: #fff;
  pointer-events: none;
  transition: .5s;
}
.time{
  
 
  font-size: 16px;
  color: #fff;
  transition: .5s;
}


.login-box .user-box input:focus ~ label,
.login-box .user-box input:valid ~ label {
  top: -20px;
  left: 0;
  color: #03e9f4;
  font-size: 12px;
}

.a {
  position: relative;
  display: inline-block;
  padding: 10px 20px;
  color: #03e9f4;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 40px;
  letter-spacing: 4px;
    background: rgba(0,0,0,.5);

}

.a:hover {
  background: #03e9f4;
  color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 5px #03e9f4,
              0 0 25px #03e9f4,
              0 0 50px #03e9f4,
              0 0 100px #03e9f4;
}
.b {
  position: relative;
  display: inline-block;
  padding: 10px 20px;
  color: #03e9f4;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 5px;
  letter-spacing: 4px;
    background: rgba(0,0,0,.5);

}

.b:hover {
  background: #03e9f4;
  color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 5px #03e9f4,
              0 0 25px #03e9f4,
              0 0 50px #03e9f4,
              0 0 100px #03e9f4;
}




}


</style>
<body>
<div class="login-box">
  <h2>จองตั๋วรถไฟ</h2>
  <form action="<?php echo site_url('Welcome/insert');?>" method="POST">
    <div class="user-box">
      <input type="text" name="t_no" required="">
      <label>ขบวน</label>
    </div>
    <div class="user-box">
      <label>ประเภทรถ</label>
      <br></br>
    
      <select name="tpl" id="cars">
  <option value="รถธรรมดา">รถธรรมดา</option>
  <option value="รถดีเซลราง">รถดีเซลราง</option>
  <option value="รถด่วนดีเซลราง">รถด่วนดีเซลราง</option>
</select>
</select>
    </div>
    <br>

      <div class="user-box">
      <input type="text" name="to_name" required="">
      <label>ต้นทาง &nbsp </label>
    </div>
      <div class="time">
      <label>เวลาออก</label>
      <input type="time" name="to_date" required="">
      
     
    </div>
      
    
     <div class="time"> 
     <br>     
     <label>จุดพัก</label><br>
<br>

      <label>เวลาถึง &nbsp  </label>
      <input type="time" name="tb_arr" required="">
      
     
    </div>

<div class="time"> 
     <br>     
   

      <label>เวลาออก</label>
      <input type="time" name="tb_out" required="">
      
     <br>
    <br>
    </div>
    <div class="user-box">
    
      <input type="text" name="td_name" required="">
      <label>ปลายทาง</label>
    </div>
     <div class="time">
      <label>ถึงเวลา  &nbsp </label>
      <input type="time" name="td_date" required="">
      
     
    </div>
   
    <input type="submit" name="submit" value="Submit" class="a"> 

    </form>
       <a href ="<?php echo site_url('Welcome/show');?>"><button class ="b">ดูตารางรถไฟ</button></a>

</div>

</body>
</html>