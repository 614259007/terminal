<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstrap');
?>
<html>
<style>
body {
  margin:0;
  padding:0;
  font-family: sans-serif;
  background: linear-gradient(#141e30, #141e30);
    color: #fff;}

    .b {
        float: left;
        width: 30%;
        margin-left:35%;
  position: relative;
  display: inline-block;
  padding: 10px 20px;
  color: #03e9f4;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 5px;
  letter-spacing: 4px;
    background: rgba(0,0,0,.5);

}

.b:hover {
  background: #03e9f4;
  color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 5px #03e9f4,
              0 0 25px #03e9f4,
              0 0 50px #03e9f4,
              0 0 100px #03e9f4;
}

</style>
<body>
<br>
<div class="container">
    <table class="table table-dark"align="center">
<thead>
<tr>
<th>ขบวน</th>
<th>สถานีต้นทาง</th>
<th>เวลาออก</th>
<th>ถึงจุดพักเวลา</th>
<th>ออกจากจุดพักเวลา</th>
<th>สถานีปลายทาง</th>
<th>ถึงเวลา</th>
<th>หมายเหตุ</th>

</tr>
</thead>
 <?php
            foreach ($query as $x) {

            ?>
<tbody>
<tr>
<td><?echo $x->T_no;?></td>
<td><?echo $x->to_name;?></td>
<td><?echo $x->to_date;?></td>
<td><?echo $x->tb_arr;?></td>
<td><?echo $x->tb_out;?></td>
<td><?echo $x->td_name;?></td>
<td><?echo $x->td_date;?></td>
<td><?echo $x->pl;?></td>


</tr>

</tbody>
            <?php }?>
</table>
 
       <a href ="<?php echo site_url('Welcome');?>"><button class ="b"> HOME</button></a>

</div>
</body>
</html>