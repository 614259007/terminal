<?php defined('BASEPATH') or exit('No direct script access allowed');
class Model_T extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

     function train($data)
    {
        $this->db->insert('train', $data);
    }

    function origin($data)
    {
        $this->db->insert('origin', $data);
    }
    function destination($data)
    {
        $this->db->insert('destination', $data);
    }
    function break_point($data)
    {
        $this->db->insert('break_point', $data);
    }

   



    function select_t()
    {


        $this->db->select('*');
        $this->db->from('train');
        $this->db->join('origin', 'origin.r_id = train.r_id', 'left');
        $this->db->join('destination', 'destination.r_id = train.r_id', 'left');
        $this->db->join('break_point', 'break_point.r_id = train.r_id', 'left');
       // $this->db->order_by('train.T_no','asc');
        $this->db->where('origin.to_name ','สุรินทร์');
        $this->db->where_in('destination.td_name','กรุงเทพ');
        $query = $this->db->get();
        return $query->result();
    }

    
    
}
